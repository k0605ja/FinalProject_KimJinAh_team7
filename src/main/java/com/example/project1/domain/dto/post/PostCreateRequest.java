package com.example.project1.domain.dto.post;

import com.example.project1.domain.entity.Post;
import com.example.project1.domain.entity.User;
import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Builder
public class PostCreateRequest {

    private String title;
    private String body;

    // User user
    public Post toEntity(User user) {
        return Post.builder()
                .title(title)
                .body(body)
                .user(user)
                .build();
    }
}