package com.example.project1.domain.dto.user;

public enum UserRole {
    USER,
    ADMIN;
}
