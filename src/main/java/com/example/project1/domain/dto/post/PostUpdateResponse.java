package com.example.project1.domain.dto.post;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Getter
@Builder // 순서 x
public class PostUpdateResponse {

    private String message;
    private Long postId;

}