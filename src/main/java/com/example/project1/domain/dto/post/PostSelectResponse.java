package com.example.project1.domain.dto.post;
import com.example.project1.domain.entity.Post;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Builder
public class PostSelectResponse {

    private Long id;
    private String title;
    private String body;
    private String userName;
    private String createdAt;
    private String lastModifiedAt;

    public PostSelectResponse (Post post) {
        this.id = post.getId();
        this.title = post.getTitle();
        this.body = post.getBody();
        this.userName = post.getUser().getUserName();
        this.createdAt = post.getRegisteredAt();
        this.lastModifiedAt = post.getUpdatedAt();
    }

}

