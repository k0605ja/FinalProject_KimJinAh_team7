package com.example.project1.service;

import com.example.project1.domain.dto.user.UserJoinRequest;
import com.example.project1.domain.entity.User;
import com.example.project1.exception.AppException;
import com.example.project1.exception.ErrorCode;
import com.example.project1.repository.UserRepository;
import com.example.project1.jwt.JwtTokenUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {

    private final UserRepository userRepository;
    private final BCryptPasswordEncoder encoder;


    @Value("${jwt.token.secret}")
    private String secretKey;
    private long expiredTimeMs = 1000 * 60 * 60; // Token 유효 시간: 1시간


    // 회원 가입
    public User join(UserJoinRequest userJoinRequest) {

        // id 중복 검사
        userRepository.findByUserName(userJoinRequest.getUserName());
        log.info("서비스 단 유저:{}",userJoinRequest);
        List<User> userList = userRepository.findByUserName(userJoinRequest.getUserName());

        if (!userList.isEmpty()) {
            throw new AppException(ErrorCode.DUPLICATED_USER_NAME,String.format("%s은 이미 가입된 이름 입니다.", userJoinRequest.getUserName()));
        }

        String encodePassword = encoder.encode(userJoinRequest.getPassword());
        // 회원 저장
        User user = userJoinRequest.toEntity(encodePassword);

        User savedUser = userRepository.save(user);
        log.info("저장된 회원 : {}",savedUser);

        // 저장 후, User로 데이터 전달
        return savedUser;
    }

    // 로그인
    public String login(String userName, String password) {

        // userName(id) 유무 확인
        User user = userRepository.findOptionalByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, "UserName을 찾을 수 없습니다."));


        // password 일치 확인
        if (!encoder.matches(password, user.getPassword())) {
            throw new AppException(ErrorCode.INVALID_PASSWORD, "패스워드가 틀렸습니다.");
        }

        // 예외 없으면, Token 발행
        return JwtTokenUtil.createToken(userName, secretKey, expiredTimeMs);
    }

}
