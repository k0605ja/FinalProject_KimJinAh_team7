package com.example.project1.service;

import com.example.project1.domain.dto.post.*;
import com.example.project1.domain.entity.Post;
import com.example.project1.domain.entity.User;
import com.example.project1.exception.AppException;
import com.example.project1.exception.ErrorCode;
import com.example.project1.repository.PostRepository;
import com.example.project1.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class PostService {

    private final PostRepository postRepository;
    private final UserRepository userRepository;
    // 포스트 등록
    public PostCreateResponse createPost(PostCreateRequest request, String userName) {
        log.info("서비스 userName:{}",userName);

        // 인증으로 들어온 userName으로 User 엔티티 확인
        User user = userRepository.findOptionalByUserName(userName)
                .orElseThrow(() -> {throw new AppException(ErrorCode.USERNAME_NOT_FOUND, "회원만 글 작성 권한이 있습니다.");});


        //  저장
        //  JpaRepository<Article,Long>를 사용하기 때문에 articleRequestDto -> Article 타입으로 변환
        Post post = request.toEntity(user);
        Post savedPost = postRepository.save(post);
        if (savedPost.getId() == null) {
            throw new RuntimeException("해당 파일은 존재하지 않습니다");
        }
        PostCreateResponse postCreateResponse = new PostCreateResponse(savedPost.getId(), "포스트 등록 완료");
        return postCreateResponse;
    }

    // 포스트 조회
    public PostSelectResponse getPost(Long postId) {
        Optional<Post> postOptional = postRepository.findById(postId);
        Post post = postOptional.orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND,"해당 글 없습니다"));
        PostSelectResponse postSelectResponse =
                new PostSelectResponse(post.getId(), post.getTitle(),
                        post.getBody(), post.getUser().getUserName(),
                        post.getRegisteredAt(), post.getUpdatedAt());
        return postSelectResponse;
    }

    // 포스트 전체 조회
    public List<PostSelectResponse> getPosts(Pageable pageable) {
        Page<Post> posts = postRepository.findAll(pageable);
        List<PostSelectResponse> postSelectResponseList =
                posts.stream().map(p -> new PostSelectResponse(p)).collect(Collectors.toList());

        return postSelectResponseList;
    }
    // 포스트 제목으로 조회
    public List<PostSelectResponse> getPostsByTitle (Pageable pageable,String title) {
        Page<Post> posts = postRepository.findByTitleContaining(pageable, title);
        List<PostSelectResponse> postSelectResponseList =
                posts.stream().map(p -> new PostSelectResponse(p)).collect(Collectors.toList());
        return postSelectResponseList;
    }


    // 포스트 수정
    public PostUpdateResponse updatePost(Long postId, PostUpdateRequest postUpdateRequest, String userName) {
        log.info("수정 요청 dto :{}", postUpdateRequest);
        Post findPost =
                postRepository.findById(postId).orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND,"해당 글 없습니다"));

        log.info("userName:{}",userName);

        User user = userRepository.findOptionalByUserName(userName)
                .orElseThrow(() -> new AppException(ErrorCode.USERNAME_NOT_FOUND, String.format("%s not founded", userName)));
        Long userId = user.getId();

        // 수정 권한 확인
        if (userId != findPost.getUser().getId()) {
            throw new AppException(ErrorCode.INVALID_PERMISSION, "해당 회원은 수정할 권한이 없습니다");
        }
        //변경감지 수정
        findPost.setTitle(postUpdateRequest.getTitle());
        findPost.setBody(postUpdateRequest.getBody());

        PostUpdateResponse postUpdateResponse = new PostUpdateResponse("포스트 수정 완료", findPost.getId());
        return postUpdateResponse;

    }

    // 포스트 삭제
    public PostDeleteResponse deletePost(Long postId, String userName) {
        Optional<Post> optionalPost = postRepository.findById(postId);
        Post post =
                optionalPost.orElseThrow(() -> new AppException(ErrorCode.POST_NOT_FOUND, "해당 글은 존재하지 않아서 삭제할 수 없습니다."));

        User user = userRepository.findOptionalByUserName(userName)
                .orElseThrow(()
                        -> new AppException(ErrorCode.USERNAME_NOT_FOUND, String.format("%s not founded", userName)));

        // 작성자 본인이 아닌 경우, 예외
        if (user.getId() != post.getUser().getId()) {
            throw new AppException(ErrorCode.INVALID_PERMISSION, "게시글 삭제 권한이 없습니다.");
        }

        postRepository.delete(post);
        PostDeleteResponse deleteResponse = new PostDeleteResponse("포스트 삭제 완료", post.getId());
        return deleteResponse;
    }
}