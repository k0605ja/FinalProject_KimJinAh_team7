package com.example.project1.controller;

import com.example.project1.domain.dto.Response;
import com.example.project1.domain.dto.post.*;
import com.example.project1.service.PostService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/posts")
@Slf4j
@RequiredArgsConstructor // 필요한 생성자

public class PostController {

    private final PostService postService;

    // 포스트 등록
    @PostMapping("")
    public Response<PostCreateResponse> add(@RequestBody PostCreateRequest postAddRequest, Authentication authentication) {
        log.info("postAddRequest : {}", postAddRequest);
        log.info("authentication.getName() : {}", authentication.getName());

        PostCreateResponse postCreateResponse = postService.createPost(postAddRequest,authentication.getName());
        return Response.success(postCreateResponse);
    }

    // 포스트 전체 조회
    @GetMapping("/api/v1/posts")
    public Response<PageImpl<PostSelectResponse>> getAll(@PageableDefault(size = 20, sort ="registeredAt",
            direction = Sort.Direction.DESC) Pageable pageable) {
        List<PostSelectResponse> posts = postService.getPosts(pageable);
        return Response.success(new PageImpl<>(posts));
    }

    // 포스트 리스트
    @GetMapping("/api/v1/posts/{postId}")
    public Response<PostSelectResponse> get(@PathVariable Long postId) {
        PostSelectResponse postSelectResponse = postService.getPost(postId);
        return Response.success(postSelectResponse);
    }

    // 포스트 수정
    @PutMapping("/api/v1/posts/{id}")
    public Response<PostUpdateResponse> update(@PathVariable Long id, @RequestBody PostUpdateRequest postUpdateRequest,Authentication authentication) {
        log.info("수정 controller :{}", postUpdateRequest);
        PostUpdateResponse postUpdateResponse = postService.updatePost(id, postUpdateRequest,authentication.getName());
        return Response.success(postUpdateResponse);
    }

    // 포스트 삭제
    @DeleteMapping("/{postsId}")
    public Response<PostDeleteResponse> delete(@PathVariable Long id, Authentication authentication) {
        PostDeleteResponse deletePost = postService.deletePost(id,authentication.getName());
        return Response.success(deletePost);
    }

}
