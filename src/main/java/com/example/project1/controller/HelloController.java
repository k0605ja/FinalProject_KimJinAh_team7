package com.example.project1.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

// localhost:8080/api/v1/hello

    @RestController
    public class HelloController {

        @GetMapping(value = "/api/v1/hello", produces = "application/json; charset=utf8")
        public String hello() {
            return "김진아";
        }

        @GetMapping("/api/v1/bye")
        public ResponseEntity<String> bye() {
            return ResponseEntity.ok().body("bye");
        }
    }


